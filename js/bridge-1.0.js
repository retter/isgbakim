var deviceType = 0; // 0-Browser 1- iOS  2- Android

function detectDeviceType() {
  if (isIphone()) {
    deviceType = 1;
  } else if (isAndroid()) {
    deviceType = 2;
  }
}

function isAndroid() {
  if (navigator.userAgent.match(/Android/i))
    return true;

  return false;
}

function isIphone() {
  if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)))
    return true;

  return false;
}



detectDeviceType();

function onJsonpCallback(data) {
  onRequestResult(data);
}

var nativeBridge = new function() {


  this.log = function(logStr) {
    if (deviceType == 0) {
      console.log(logStr);
    } else if (deviceType == 1) {

    } else {
      console.log(logStr);
      //AndroidDevice.log(logStr);
    }

  }

  this.enableBack = function() {

    if (deviceType == 0) {

    } else if (deviceType == 1) {

    } else {
      console.log('enableBack called in bridge');
      AndroidDevice.enableBack();
    }
  }
  this.disableBack = function() {

    if (deviceType == 0) {

    } else if (deviceType == 1) {

    } else {
      console.log('disableBack called in bridge');
      AndroidDevice.disableBack();
    }
  }

  this.cancelScan = function() {
    if (deviceType == 0) {

    } else if (deviceType == 1) {

    } else {
      AndroidDevice.cancelScan();
    }
  }

  this.scanBarcode = function(data) {

    if (deviceType == 0) {

      var code = prompt("Enter barcode", "21712603518");
      onBarcodeResult(code);

      //setTimeout(function () {
      //    onBarcodeResult('21712603518');
      //}, 500);

    } else if (deviceType == 1) {

    } else {
      AndroidDevice.scanBarcode();
    }

  }



  this.showAlert = function(message) {
    if (deviceType == 0) {
      alert(message);
    } else if (deviceType == 1) {

    } else {
      AndroidDevice.showAlert(message);
    }
  }

  this.showLoading = function(message) {
    if (deviceType == 0) {
      $rootScopeVar.progressMessage = message;
      $rootScopeVar.showLoadingDialog = true;
    } else if (deviceType == 1) {

    } else {
      AndroidDevice.showLoading(message);
    }
  }

  this.hideLoading = function() {
    if (deviceType == 0) {
      $rootScopeVar.showLoadingDialog = false;
    } else if (deviceType == 1) {} else {
      AndroidDevice.hideLoading();
    }
  }

  this.makeHttpRequest = function(url, postData) {
    if (deviceType == 0) {

      $.ajax({
        url: url,
        method: 'post',
        contentType: "application/json",
        data: JSON.stringify(postData),
        success: function(data) {
          console.log('this.makeHttpRequest success:', data)
          onJsonpCallback(data);
        }

      });

      //$.ajax({
      //    url: url,
      //    dataType: 'jsonp',
      //    jsonpCallback: 'onJsonpCallback'

      //});

    } else if (deviceType == 1) {

    } else {

      console.log('making request with data: ' + JSON.stringify(postData));

      AndroidDevice.makeHttpRequest(url, postData != null ? 'post' : 'get', JSON.stringify(postData));
    }
  }


  //(title, signatureType, name, lastname, passbutton, callback)
  this.requestSignature = function(title, orderNumber, signatureType, passbutton, name) {
    if (deviceType == 0) {

    } else if (deviceType == 2) {

      AndroidDevice.requestSignature(title, orderNumber, signatureType, passbutton, name);
    }
  }



  this.sendJsonToiOS = function(params) {
    var JSONString = (JSON.stringify(params));
    var URLString = escape(JSONString);
    var uri = 'js://' + '' + "" + URLString;

    var iframe = document.createElement("iframe");
    iframe.src = uri;

    document.body.appendChild(iframe);
    document.body.removeChild(iframe);
  }
}
