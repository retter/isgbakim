﻿
var bakimApp = angular.module('bakimApp', ['ui.router', 'isgBakim.services']);
bakimApp.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});

var $rootScopeVar;



//bakimApp.constant("SAPWSURL", "http://172.16.1.125:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=350");
//bakimApp.constant("SAPWSURL", "http://172.16.1.129:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=350");
//bakimApp.constant("SAPWSURL", "http://sawerpdev.sgia.aero:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=350");

//bakimApp.constant("SAPWSURL", "http://172.16.1.125:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=900");
bakimApp.constant("SAPWSURL", "http://172.16.1.121:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=900");
// bakimApp.constant("SAPWSURL", "http://172.16.1.125:8000/sap/bc/icf/zpmws/{COMMAND}?format=json&sap-client=900");

bakimApp.constant("StatusCodes", {
  E0003: "ONAYLANDI",
  E0004: "İŞLEMDE",
  E0005: "DAHİLİ İŞÇİLİK BEKLENİYOR",
  E0006: "MALZEME BEKLENİYOR",
  E0007: "İŞLETME ŞARTI BEKLENİYOR",
  E0010: "SERVİS EKİBİ BEKLENİYOR"
});

bakimApp.run(function($state, $rootScope, $http) {
  $rootScopeVar = $rootScope;
  $rootScope.isScanning = false;

  //$http.defaults.headers.common.Authorization = 'Basic SlNPTjoxNTlxc2MtKw=='

  //FastClick.attach(document.body);
});

bakimApp.service('httpRequestInterceptor', ['$rootScope', function($rootScope) {

  var service = {
    request: function($config) {

      if ($config.method === 'GET' && $config.url.indexOf("sawerpdev") != -1) {
        console.log('SAP REQUEST')
      }

      $rootScope.httpRequestInProgress = true;
      //$config.headers = {'Authentication': 'Basic ' + btoa('JSON:159qsc-+')}
      return $config;
    }
  };
  return service;
}]);
bakimApp.service('myHttpInterceptor', function($q, $rootScope) {
  var service = {
    response: function(response) {
      $rootScope.httpRequestInProgress = false;
      return response;
    },
    responseError: function(response) {
      // do something on error
      return $q.reject(response);
    }
  };
  return service;
});


bakimApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
  function($stateProvider, $urlRouterProvider, $httpProvider) {

    //$httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + btoa('JSON:159qsc-+');
    //$httpProvider.defaults.headers.common['Baran'] = 'Basic ' + btoa('JSON:159qsc-+');

    $httpProvider.interceptors.push('httpRequestInterceptor');
    $httpProvider.interceptors.push('myHttpInterceptor');

    $urlRouterProvider.otherwise("/welcome");

    // Now set up the states
    $stateProvider
      .state('welcome', {
        url: "/welcome",
        templateUrl: "partials/welcome.html"

      })
      .state('login', {
        url: "/login",
        templateUrl: "partials/login.html",
        controller: "LoginController"
      }).state('home', {
        url: "/home",
        templateUrl: "partials/home.html",
        controller: "HomeController"
      }).state('home.search', {
        url: "^/search",
        templateUrl: "partials/search.html",
        controller: "SearchController",
        onEnter: function() {
          console.log('onEnter');
          console.log('nativeBridge: ' + nativeBridge);
          nativeBridge.disableBack();
        },
        onExit: function() {
          console.log('onExit');
          nativeBridge.enableBack();
        }
      }).state('home.searchresults', {
        url: "^/searchresults?sipno&siptype&sefliktype&code&ekipman&mahal&startDate&endDate&isemriStatus&teknikBirim",
        templateUrl: "partials/searchresults.html",
        controller: "SearchResultsController"
      }).state('home.sipdetails', {
        url: "^/sipdetails?sipid",
        templateUrl: "partials/sipdetails.html",
        controller: "SipDetailsController"
      }).state('home.sipdetails.islemdetails', {
        url: "^/sipdetails/islemdetails?sipid&islemid",
        templateUrl: "partials/islemdetails.html",
        controller: "IslemDetailsController"
      }).state('home.sipdetails.sipcause', {
        url: "^/sipdetails/sipcause?sipid",
        templateUrl: "partials/sipcause.html",
        controller: "SipCauseController"
      }).state('home.sipdetails.selectislemstatus', {
        url: "^/sipdetails/selectislemstatus?sipid&islemid",
        templateUrl: "partials/selectislemstatus.html",
        controller: "SelectIslemStatusController"
      });

  }
]);

bakimApp.run(['$rootScope',
  function($rootScope) {
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {

        if (!$rootScope.userid && fromState.name == 'login') {
          event.preventDefault();
        }


      })
  }
]);


bakimApp.filter('groupBy', function() {
  return function(list, group_by) {

    var filtered = [];
    var prev_item = null;
    var group_changed = false;
    // this is a new field which is added to each item where we append "_CHANGED"
    // to indicate a field change in the list
    var new_field = group_by + '_CHANGED';

    // loop through each item in the list
    angular.forEach(list, function(item) {

      group_changed = false;

      // if not the first item
      if (prev_item !== null) {

        // check if the group by field changed
        if (prev_item[group_by] !== item[group_by]) {
          group_changed = true;
        }

        // otherwise we have the first item in the list which is new
      } else {
        group_changed = true;
      }

      // if the group changed, then add a new field to the item
      // to indicate this
      if (group_changed) {
        item[new_field] = true;
      } else {
        item[new_field] = false;
      }

      filtered.push(item);
      prev_item = item;

    });

    return filtered;
  };
})


function onBarcodeResult(code) {
  $rootScopeVar.$broadcast('onBarcodeScanned', code);
}

function onRequestResult(result) {
  if (typeof result == 'string') {
    console.log('result in JS: ' + result);
    result = JSON.parse(result);
  }
  $rootScopeVar.$broadcast('onRequestResult', result);
}

function onSignatureResult(result) {
  if (typeof result == 'string') {
    console.log('result in JS: ' + result);
    //result = JSON.parse(result);
  }
  $rootScopeVar.$broadcast('onSignatureResult', result);
}
