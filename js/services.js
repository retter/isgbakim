﻿
var servicesModule = angular.module('isgBakim.services', []);

servicesModule.factory('barcodeScanner', ['$rootScope', '$timeout', function($rootScope, $timeout) {

  var factory = {};

  $rootScope.$on('onBarcodeScanned', function(e, args) {
    //$rootScope.$apply(function () {
    $timeout(function() {
      $rootScope.isScanning = false;
      if ($rootScope.scanCallback) {
        $rootScope.scanCallback(args);
      }
    }, 1);

    //});
  });

  factory.scanBarcode = function(callback, title) {
    $rootScope.barcodeTitle = title ? title : 'BARKOD TARANIYOR';
    $rootScope.scanCallback = callback;
    $rootScope.isScanning = true;
    nativeBridge.scanBarcode();

  };

  return factory;
}]);


servicesModule.factory('nativeHttpRequestService', ['$rootScope', function($rootScope) {

  var factory = {};

  $rootScope.$on('onRequestResult', function(e, args) {
    $rootScope.$apply(function() {
      if ($rootScope.httpRequestResultCallback) {
        $rootScope.httpRequestResultCallback(args);
      }
    });
  });

  factory.makeHttpRequest = function(url, data, callback) {
    $rootScope.httpRequestResultCallback = callback;

    nativeBridge.makeHttpRequest(url, data);
  };

  return factory;
}]);

servicesModule.factory('signatureService', ['$rootScope', function($rootScope) {

  var factory = {};

  $rootScope.$on('onSignatureResult', function(e, args) {
    console.log('onSignatureResult');
    $rootScope.$apply(function() {
      if ($rootScope.requestSignatureCallback) {
        $rootScope.requestSignatureCallback(args);
      }
    });
  });

  factory.requestSignature = function(title, orderNumber, signatureType, passbutton, name, callback) {
    $rootScope.requestSignatureCallback = callback;

    nativeBridge.requestSignature(title, orderNumber, signatureType, passbutton, name);
  };

  return factory;
}]);


servicesModule.factory('sapWsCommand', ['$rootScope', 'SAPWSURL', function($rootScope, SAPWSURL) {

  var factory = {};


  factory.getUrl = function(command) {
    return SAPWSURL.replace('{COMMAND}', command);
  };

  return factory;
}]);
