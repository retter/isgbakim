﻿
var bakimApp = angular.module('bakimApp');

function JSON_CALLBACK(data) {
  console.log('JSON_CALLBACK: ' + JSON.stringify(data));
}

bakimApp.controller('AppController', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state) {

  // $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
  //     console.log('fromState: ' + fromState.name + ' toState: ' + toState.name);
  //     if(fromState.name == 'home.search') {
  //         if(toState.name != 'home.searchresults' && toState.name != 'login') {
  //             event.preventDefault();
  //         }
  //     }
  //  });

}]);

bakimApp.controller('LoginController', ['$scope', '$rootScope', 'barcodeScanner', '$http', '$state', 'nativeHttpRequestService', 'sapWsCommand', 'signatureService',
  function($scope, $rootScope, barcodeScanner, $http, $state, nativeHttpRequestService, sapWsCommand, signatureService) {

    console.log('LoginController')

    $scope.scan = function() {

      // signatureService.requestSignature("Imza Giriniz", '110002347', 'T', true, 'Baran Baygan', function(filename) {
      //
      //   nativeBridge.showAlert('Filename: ' + filename);
      //
      // });
      // return;

      console.log('LoginController scan called')
      barcodeScanner.scanBarcode(function(code) {
        $scope.barcode = code;

        console.log('calling nativeHttpRequestService')
        nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_PERSONEL_DETAY'), {
          I_PERSONEL: code
        }, function(data) {

          if (data.E_SONUC == 'NOTOK') {
            $scope.barcode = data.E_MESSAGE;
          } else {
            if (data.T_PM_RFID_PER_INFO.MERNI == code) {

              $rootScope.username = data.T_PM_RFID_PER_INFO.VORNA + ' ' + data.T_PM_RFID_PER_INFO.NACHN;
              $rootScope.userid = data.T_PM_RFID_PER_INFO.MERNI;
              $rootScope.userseflik = data.E_SEFLIK;

              $state.go('home.search');
            }
          }


        });

      }, 'Kimlik Kartı Tarat');
    };
  }
]);

bakimApp.controller('HomeController', ['$rootScope', '$scope', '$state', '$rootScope', 'nativeHttpRequestService', 'sapWsCommand', function($rootScope, $scope, $state, $rootScope, nativeHttpRequestService, sapWsCommand) {
  $scope.logout = function() {
    if (confirm('Çıkış yapmak istiyor musunuz?')) {

      $state.go('login', {}, {
        location: 'replace'
      });

      //history.go(-(history.length - 1));

      $rootScope.userid = null;
    }
  }


  // $rootScope.currentIsEmri = null
  // setTimeout(()=>{
    
  //   nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_FIND_AUFNR'), {
  //     I_MERNI: $rootScope.userid
  //   }, function(data) {
  //     console.log('ZPM_RFID_FIND_AUFNR result', data)
  //     if(data && data.T_AUFNR && data.T_AUFNR !== '') {
        
  //       $rootScope.currentIsEmri = data.T_AUFNR
  //       console.log('ZPM_RFID_FIND_AUFNR $rootScope.currentIsEmri', $rootScope.currentIsEmri)
  //     }
  //   });
  // }, 1000)
  




}]);

bakimApp.controller('ScanningWindowController', ['$scope', '$rootScope', function($scope, $rootScope) {
  $scope.cancelScan = function() {
    $rootScope.isScanning = false;
    nativeBridge.cancelScan();

  };
}]);


bakimApp.controller('SearchController', ['$scope', '$rootScope', 'barcodeScanner', '$state', 'sapWsCommand', 'nativeHttpRequestService',
  function($scope, $rootScope, barcodeScanner, $state, sapWsCommand, nativeHttpRequestService) {

    $scope.dateType = 'last15';
    // if($rootScope.userseflik) {
    //     $scope.sefliktype = $rootScope.userseflik;
    // }


    $rootScope.currentIsEmri = null
    setTimeout(()=>{
      
      nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_FIND_AUFNR'), {
        I_MERNI: $rootScope.userid
      }, function(data) {
        console.log('ZPM_RFID_FIND_AUFNR result', data)
        if(data && data.T_AUFNR && data.T_AUFNR !== '') {
          
          $rootScope.currentIsEmri = data.T_AUFNR
          console.log('ZPM_RFID_FIND_AUFNR $rootScope.currentIsEmri', $rootScope.currentIsEmri)
        }
      });

    }, 1000)    

    $scope.scan = function() {
      barcodeScanner.scanBarcode(function(code) {

        if (code.indexOf('/') != -1) {
          // Mahal
          $state.go('home.searchresults', {
            sefliktype: $scope.sefliktype,
            siptype: $scope.siptype,
            mahal: code,
            isemriStatus: $scope.isemriStatus
          });
        } else {
          // Ekipman/teknik nesne
          $state.go('home.searchresults', {
            sefliktype: $scope.sefliktype,
            siptype: $scope.siptype,
            ekipman: code,
            isemriStatus: $scope.isemriStatus
          });
        }


      });
    };

    function addLeadingZero(datePart) {
      if (datePart.length == 1) return '0' + datePart;
      return datePart;
    }

    function getStartDate() {

      if ($scope.dateType == 'all') return null;

      if ($scope.dateType == 'selectdate') {
        return $('input[name="startDate_submit"]').val();
      }
      var d = new Date();

      if ($scope.dateType == 'last15') {
        var dateOffset = (24 * 60 * 60 * 1000) * 15; //15 days
        d.setTime(d.getTime() - dateOffset);
      }

      return d.getFullYear() + addLeadingZero(String((d.getMonth() + 1))) + addLeadingZero(String(d.getDate()));
    }

    function getEndDate() {

      if ($scope.dateType == 'all') return null;

      if ($scope.dateType == 'selectdate') {
        return $('input[name="endDate_submit"]').val();
      }
      var d = new Date();
      return d.getFullYear() + addLeadingZero(String((d.getMonth() + 1))) + addLeadingZero(String(d.getDate()));
    }

    $scope.searchClicked = function() {
      $state.go('home.searchresults', {
        sefliktype: $scope.sefliktype,
        siptype: $scope.siptype,
        startDate: getStartDate(),
        endDate: getEndDate(),
        isemriStatus: $scope.isemriStatus,
        teknikBirim: $scope.teknikBirim
      });
      //$state.go('home.searchresults', { sefliktype: 'ELE', siptype: 'PM01' });
    };

    nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_SEFLIKLER'), {}, function(data) {
      $scope.seflikTypes = data.T_INGPR.split(',');
      $scope.seflikTypes.unshift('');

      nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_SIPARIS_TURU'), {}, function(data) {
        $scope.sipTypes = data.T_AUART.split(',');

        $scope.sefliktype = $rootScope.userseflik;
      });
    });

    $('.datepicker').pickadate({
      formatSubmit: 'yyyymmdd',
      hiddenName: true
    });
  }
]);

bakimApp.controller('SearchResultsController', ['$scope', '$rootScope', '$stateParams', 'sapWsCommand', 'nativeHttpRequestService', '$state', '$http',
  function($scope, $rootScope, $stateParams, sapWsCommand, nativeHttpRequestService, $state, $http) {

    $scope.sipno = $stateParams.sipno == null ? '' : $stateParams.sipno;
    $scope.siptype = $stateParams.siptype == null ? '' : $stateParams.siptype;
    $scope.sefliktype = $stateParams.sefliktype == null ? '' : $stateParams.sefliktype;
    $scope.ekipman = $stateParams.ekipman == null ? '' : $stateParams.ekipman;
    $scope.mahal = $stateParams.mahal == null ? '' : $stateParams.mahal;
    $scope.isemriStatus = $stateParams.isemriStatus == null ? '' : $stateParams.isemriStatus;
    $scope.teknikBirim = $stateParams.teknikBirim == null ? '' : $stateParams.teknikBirim;

    if ($scope.teknikBirim != '') {
      $scope.mahal = $scope.teknikBirim;
    }

    $scope.searching = true;
    nativeBridge.showLoading('yukleniyor');

    nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_ISEMRI'), {
      I_AUFNR: $scope.sipno,
      I_INGPR: $scope.sefliktype,
      I_AUART: $scope.siptype,
      I_DATUM: $stateParams.startDate == null || $stateParams.endDate == null ? '' : {
        TARIH1: $stateParams.startDate,
        TARIH2: $stateParams.endDate
      },
      I_EQUNR: $scope.ekipman != '' ? $scope.ekipman : '',
      I_STRNO: $scope.mahal != '' ? [{
        TPLNR: $scope.mahal
      }] : '',
      I_STATUS: $scope.isemriStatus

    }, function(data) {

      $scope.searching = false;
      nativeBridge.hideLoading();

      if (data.T_ISEMRI.length == 0) {
        nativeBridge.showAlert('İş emri bulunamadı');
      }

      //nativeBridge.log('data search: ' + data.T_ISEMRI);

      $scope.data = data.T_ISEMRI;
    });

    $scope.sipClicked = function(sipId) {
      $state.go('home.sipdetails', {
        sipid: sipId
      });
    };

  }
]);


bakimApp.controller('SipDetailsController', ['$scope', '$rootScope', '$stateParams', 'sapWsCommand', 'nativeHttpRequestService', '$state', '$http', 'StatusCodes',
  function($scope, $rootScope, $stateParams, sapWsCommand, nativeHttpRequestService, $state, $http, StatusCodes) {

    $rootScope.isStarted = false;
    $scope.showIslemList = true;
    $scope.sipid = $stateParams.sipid;
    $rootScope.selectedSip = {}
    $rootScope.selectedSip.ekipmanListStr = ''

    // Mark all subitems as false. Because this might stay in rootScope as true from a previous session.
    $rootScope[$stateParams.sipid + '_allSubItemsChecked'] = false;

    function onStateChangeStart(event, toState, toParams, fromState, fromParams) {
      console.log('$stateChangeStart in SipDetailsController');
      if (toState.name == 'home.sipdetails') {
        $scope.showIslemList = true;
      } else {
        $scope.showIslemList = false;
      }
    };

    

    

    $scope.showAlert = function(txt) {
      nativeBridge.showAlert(txt);
    }

    var unregisterStateChangeStart = $rootScope.$on('$stateChangeStart', onStateChangeStart);
    $scope.$on('$destroy', function() {
      unregisterStateChangeStart();
    });


    


    nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_ISLEMLISTESI'), {
      I_AUFNR: $scope.sipid
    }, function(data) {
      if (data.E_SONUC == 'OK') {
        $scope.data = data;
        $rootScope.selectedSip = data;
        $scope.statusText = StatusCodes[data.E_STATUS];


        nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_FIND_ORDER_EQUNR'), {
          I_ORDER: $scope.sipid
        }, function(data) {
          console.log("ZPM_RFID_FIND_ORDER_EQUNR data")
          let ekipmanListStr = ''
          for(let ekip of data.T_LIST) {
            ekipmanListStr += ekip.EQUNR + ' ' + ekip.EQKTX + '\n'
          }
          // console.log("ekipmanListStr", ekipmanListStr)
          $rootScope.selectedSip.ekipmanListStr = ekipmanListStr
        });

      } else {
        nativeBridge.showAlert(data.E_MESSAGE);
      }

      // Fill islem personel
      var islemPersonel = [];
      if (data.T_PERLOG)
        for (var i = 0; i < data.T_PERLOG.length; i++) {
          var per = data.T_PERLOG[i];
          if (per.AUFNR == $stateParams.sipid && per.MERNI != '99999999999') {
            islemPersonel.push(per);
          }
        }

      if (islemPersonel.length == 0) {
        //$scope.isstarted = false;
        $rootScope.isStarted = false;
      } else {
        //$scope.isstarted = true;
        $rootScope.isStarted = true;
      }
    });




    $scope.islemClicked = function(islem) {
      console.log('isstarted: ' + $rootScope.isStarted);
      if (islem.DISABLE == 'X') return;

      if (islem.AVORNR != '' && islem.AVORNR != null) {

        //if(islem.ISSTARTED.indexOf("1") == -1) return;
        if ($rootScope.isStarted == false) return;

        islem.checked = islem.checked == null ? true : !islem.checked;
        // Check if all sub items are checked and save that to RootScope
        $rootScope[$scope.sipid + '_allSubItemsChecked'] = true;
        for (var i = 0; i < $scope.data.T_ISLIST.length; i++) {
          // Check if this is a sub item
          if (!$scope.data.T_ISLIST[i].checked && $scope.data.T_ISLIST[i].AVORNR != null && $scope.data.T_ISLIST[i].AVORNR != '') {
            // There is at least 1 item not checked
            $rootScope[$scope.sipid + '_allSubItemsChecked'] = false;
            break;
          }
        }

        return;
      }

      $state.go('home.sipdetails.islemdetails', {
        sipid: $stateParams.sipid,
        islemid: islem.VORNR
      });
    };

  }
]);

bakimApp.controller('SipCauseController', ['$scope', '$rootScope', '$stateParams', 'sapWsCommand', 'nativeHttpRequestService', '$state', '$http', 'barcodeScanner',
  function($scope, $rootScope, $stateParams, sapWsCommand, nativeHttpRequestService, $state, $http, barcodeScanner) {

    nativeBridge.showLoading('Yukleniyor');
    $scope.selectedSip = $rootScope.selectedSip;

    $scope.getDamageLabel = function(damageObj) {
      //return '';
      return damageObj.CODE + ' ' + damageObj.KURZTEXT;
    }

    $scope.getDamageCodeGroupLabel = function(group) {
      return group.CODEGRUPPE + ' ' + group.KURZTEXT_DESC;
    }

    $scope.$watch('selectedDamage', function() {

    });

    $scope.groupChanged = function() {


      console.log("groupChanged: " + $scope.selectedDamageGroup);

      $scope.damageCodesForSelectedGroup = [];
      for (var i = 0; i < $scope.damageCodes.length; i++) {
      //for (var i = $scope.damageCodes.length - 1; i >= 0; i--) {
        if ($scope.damageCodes[i].CODEGRUPPE == $scope.selectedDamageGroup.CODEGRUPPE) {
          $scope.damageCodesForSelectedGroup.push($scope.damageCodes[i]);
        }
      };
    };

    $scope.saveClicked = function() {

      if ($scope.selectedDamage == null || $scope.selectedNeden == null) {
        nativeBridge.showAlert('Lütfen hasar ve neden kodlarını giriniz');
        return;
      }

      nativeBridge.showLoading('Yükleniyor');

      console.log('CALLING ZPM_RFID_UPDATE_NOTIF with params:');
      console.log('$rootScope.selectedSip.E_AUFNR: ' + $rootScope.selectedSip.E_AUFNR);
      console.log('$scope.selectedDamage.CODEGRUPPE: ' + $scope.selectedDamage.CODEGRUPPE);
      console.log('$scope.selectedDamage.CODE: ' + $scope.selectedDamage.CODE);
      console.log('$scope.selectedNeden.CODEGRUPPE: ' + $scope.selectedNeden.CODEGRUPPE);
      console.log('$scope.selectedNeden.CODE: ' + $scope.selectedNeden.CODE);


      nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_UPDATE_NOTIF'), {
        I_ORDER: $rootScope.selectedSip.E_AUFNR,
        I_FEGRP: $scope.selectedDamage.CODEGRUPPE,
        I_FECOD: $scope.selectedDamage.CODE,
        I_URGRP: $scope.selectedNeden.CODEGRUPPE,
        I_URCOD: $scope.selectedNeden.CODE
      }, function(data) {

        nativeBridge.hideLoading();

        if (data.E_SONUC != 'OK') {
          nativeBridge.showAlert('KAYIT EDILEMEDI: ' + data.E_MESSAGE);
        } else {

          $rootScope.selectedSip.T_CAUSE[0].FECOD = $scope.selectedDamage.CODE;
          $rootScope.selectedSip.T_CAUSE[0].FEGRP = $scope.selectedDamage.CODEGRUPPE;
          $rootScope.selectedSip.T_CAUSE[0].URCOD = $scope.selectedNeden.CODE;
          $rootScope.selectedSip.T_CAUSE[0].URGRP = $scope.selectedNeden.CODEGRUPPE;

          window.history.back();
        }
      });


    };

    nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_DAMAGE_CAUSE'), {
      I_INGRP: $rootScope.selectedSip.E_INGPR,
      I_KATALOGART: 'Z'
    }, function(data) {


      console.log("damageCodes: " + JSON.stringify(data.T_DAMAGE));
      $scope.damageCodes = data.T_DAMAGE;
      $scope.damageCodeGroups = [];
      var addedCodeGroups = [];

      for (var i = 0; i < $scope.damageCodes.length; i++) {

        if (addedCodeGroups.indexOf($scope.damageCodes[i].CODEGRUPPE) == -1) {
          addedCodeGroups.push($scope.damageCodes[i].CODEGRUPPE);
          $scope.damageCodeGroups.push($scope.damageCodes[i]);
        }

        // if ($scope.damageCodeGroups.indexOf($scope.damageCodes[i]) == -1) {
        //   //if ($scope.damageCodeGroups.indexOf($scope.damageCodes[i].CODEGRUPPE) == -1) {
        //   //$scope.damageCodeGroups.push($scope.damageCodes[i].CODEGRUPPE + " " + $scope.damageCodes[i].KURZTEXT_DESC);
        //   $scope.damageCodeGroups.push($scope.damageCodes[i]);
        // }
      }

      // $scope.$apply(function () {
      //     $scope.damageCodeGroups = $scope.damageCodeGroups;
      // });

      nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_DAMAGE_CAUSE'), {
        I_INGRP: $rootScope.selectedSip.E_INGPR,
        I_KATALOGART: '5'
      }, function(data) {
        $scope.nedenCodes = data.T_DAMAGE;

        // SELECT damage and neden items in select boxes
        if ($rootScope.selectedSip && $rootScope.selectedSip.T_CAUSE && $rootScope.selectedSip.T_CAUSE.length > 0) {

          $scope.selectedDamage = null;
          $scope.selectedNeden = null;

          angular.forEach($scope.damageCodes, function(value, key) {
            if (value.CODEGRUPPE == $rootScope.selectedSip.T_CAUSE[0].FEGRP &&
              value.CODE == $rootScope.selectedSip.T_CAUSE[0].FECOD) {
              $scope.selectedDamage = value;
            }
          });

          angular.forEach($scope.nedenCodes, function(value, key) {
            if (value.CODEGRUPPE == $rootScope.selectedSip.T_CAUSE[0].URGRP &&
              value.CODE == $rootScope.selectedSip.T_CAUSE[0].URCOD) {
              //$scope.selectedNeden = value;
              $scope.selectedNedenText = value.KURZTEXT;
            }
          });
        }

        nativeBridge.hideLoading();
      });


    });

  }
]);


bakimApp.controller('IslemDetailsController', ['$scope', '$rootScope', '$stateParams', 'sapWsCommand', 'nativeHttpRequestService', '$state', '$http', 'barcodeScanner', '$timeout',
  function($scope, $rootScope, $stateParams, sapWsCommand, nativeHttpRequestService, $state, $http, barcodeScanner, $timeout) {

    $scope.islemPersonel = [];


    function getSiparis() {
      $scope.islemPersonel = [];
      nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_ISLEMLISTESI'), {

        I_AUFNR: $stateParams.sipid

      }, function(data) {

        $scope.data = data;
        $scope.selectedSip = data;

        console.log('selected sip: ' + JSON.stringify($scope.selectedSip));

        if (data.T_ISLIST)
          for (var i = 0; i < data.T_ISLIST.length; i++) {
            var entry = data.T_ISLIST[i];
            if (entry.VORNR == $stateParams.islemid) {
              $scope.selectedIslem = entry;
              if ($scope.selectedIslem.ISSTARTED.indexOf("1") != -1) {
                $scope.isstarted = true;
                $rootScope.isStarted = true;
              } else {
                $scope.isstarted = false;
                $rootScope.isStarted = false;
              }
              break;
            }
          }

        // Fill islem personel
        if (data.T_PERLOG)
          for (var i = 0; i < data.T_PERLOG.length; i++) {
            var per = data.T_PERLOG[i];
            if (per.VORNR == $stateParams.islemid && per.MERNI != '99999999999') {
              $scope.islemPersonel.push(per);
            } else if (per.MERNI == '99999999999') {

            }
          }

        // if ($scope.islemPersonel.length == 0) {
        //     $scope.isstarted = false;
        //     $rootScope.isStarted = false;
        // } else {
        //     $scope.isstarted = true;
        //     $rootScope.isStarted = true;
        // }
      });
    }

    getSiparis();

    $scope.addPersonel = function() {
      $scope.scanForPersonel();
    };

    $scope.removePersonel = function() {
      barcodeScanner.scanBarcode(function(code) {
        nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_ISBITIR'), {
            I_AUFNR: $rootScope.selectedSip.E_AUFNR,
            I_VORNR: $scope.selectedIslem.VORNR,
            T_TEYIT: [{
              AUFNR: $rootScope.selectedSip.E_AUFNR,
              VORNR: $scope.selectedIslem.VORNR
            }],
            I_MERNI: [{
              MERNI: code
                //IEDD: '20140717',
                //IEDZ: '160000'
            }]
          },
          function(data) {
            if (data.E_SONUC == 'OK') {
              getSiparis();
            } else {
              if (data.T_RETURN && data.T_RETURN[0])
                nativeBridge.showAlert(data.T_RETURN[0].MESAJ);
            }
          });
      });
    };
    $scope.finishIslem = function() {

      console.log('_allSubItemsChecked: ' + $rootScope[$stateParams.sipid + '_allSubItemsChecked']);

      if ($scope.data.E_AUART == 'PM02') {
        if (!$rootScope[$stateParams.sipid + '_allSubItemsChecked']) {
          nativeBridge.showAlert('BUTUN ALT ITEMLAR CHECK OLMALI');
          return;
        }
      }

      barcodeScanner.scanBarcode(function(code) {
        if (code != $scope.selectedSip.E_STRNO && $scope.selectedSip.E_EQUNR.indexOf(code) == -1) {
          nativeBridge.showAlert("MAHAL YA DA EKIPMAN NUMARASI HATALI YA DA İŞ EMRİ İLE UYUŞMUYOR");
          return;
        }
        $state.go('home.sipdetails.selectislemstatus', {
          sipid: $stateParams.sipid,
          islemid: $stateParams.islemid
        });
      }, 'MAHAL YA DA EKIPMAN TARAT');

    };
    $scope.startIslem = function() {
      $scope.scanForPersonel();
    };

    $scope.showIslemSubItems = function() {
      console.log('showIslemSubItems');

      $state.go('home.sipdetails', {
        sipid: $stateParams.sipid
      });
    };

    $scope.scanForPersonel = function() {


      barcodeScanner.scanBarcode(function(code) {

        if (code != $scope.selectedSip.E_STRNO && $scope.selectedSip.E_EQUNR.indexOf(code) == -1) {
          nativeBridge.showAlert("MAHAL YA DA EKIPMAN NUMARASI HATALI YA DA İŞ EMRİ İLE UYUŞMUYOR");
          return;
        }

        $timeout(function() {

          barcodeScanner.scanBarcode(function(code) {

            nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_ISBASLA'), {
                I_AUFNR: $rootScope.selectedSip.E_AUFNR,
                I_VORNR: $scope.selectedIslem.VORNR,
                I_MERNI: [{
                  MERNI: code
                }]
              },
              function(data) {
                if (data.E_SONUC == 'OK') {
                  getSiparis();
                } else {
                  if (data.T_RETURN && data.T_RETURN[0])
                    nativeBridge.showAlert(data.T_RETURN[0].MESAJ);
                }
              });

          }, 'PERSONEL TARAT');
        }, 1000);




      }, 'MAHAL YA DA EKIPMAN TARAT');



    };

  }
]);


bakimApp.controller('SelectIslemStatusController', ['$scope', '$rootScope', '$stateParams', 'sapWsCommand', 'nativeHttpRequestService', '$state', '$http', 'barcodeScanner', 'signatureService',
  function($scope, $rootScope, $stateParams, sapWsCommand, nativeHttpRequestService, $state, $http, barcodeScanner, signatureService) {

    $scope.showDescription = false;
    $scope.cancel = function() {

      $state.go('home.sipdetails', {
        sipid: $stateParams.sipid
      });
    };

    function updateIslemStatus(statusCode, description) {

      if ((description == '' || description == null) && statusCode != 'E0011' && $rootScope.selectedSip.E_AUART == 'PM01') {
        nativeBridge.showAlert('LÜTFEN İŞLEM AÇIKLAMASI GİRİNİZ');
        return;
      }

      if ($rootScope.selectedSip.E_AUART == 'PM01' && statusCode == 'E0011' &&
        ($rootScope.selectedSip.T_CAUSE[0].FECOD == '' || $rootScope.selectedSip.T_CAUSE[0].FEGRP == '' ||
          $rootScope.selectedSip.T_CAUSE[0].URCOD == '' || $rootScope.selectedSip.T_CAUSE[0].URGRP == '')) {

        nativeBridge.showAlert('HATA VE NEDEN KODLARI BOŞ BIRAKILAMAZ');

        return;
      }



      if(statusCode=='E0011') {
        signatureService.requestSignature("Teknisyen Imzası", $rootScope.selectedSip.E_AUFNR, 'T', false, $rootScope.username, function(technicianFilename) {

          if(technicianFilename=='cancelled'||technicianFilename=='passed'||technicianFilename=='') {
            nativeBridge.showAlert('Teknisyen imzası zorunludur');
            return;
          }

          signatureService.requestSignature("Müşteri Imzası", $rootScope.selectedSip.E_AUFNR, 'M', true, '', function(customerFilename) {

            if(customerFilename!='cancelled') {
              doUpdateOrder(technicianFilename, customerFilename);
            }


          });
        });
      } else {
        doUpdateOrder();
      }




      function doUpdateOrder(tSigFilename, cSigFilename) {

        nativeBridge.showLoading('Kayıt ediliyor');

        let rejectedStatus = '0';

        if(cSigFilename=='rejected') rejectedStatus = '1';
        if(cSigFilename=='passed' || cSigFilename=='rejected') cSigFilename = '';
        

        //description = description.replace('ç','c').replace('ö','o').replace('ı','i').replace('ş','s').replace('ü','u').replace('ğ','g');
        //description = description.replace('Ç','C').replace('Ö','O').replace('İ','I').replace('Ş','S').replace('Ü','U').replace('Ğ','G');

        nativeHttpRequestService.makeHttpRequest(sapWsCommand.getUrl('ZPM_RFID_UPDATE_ORDER'), {
          I_ORDER: $rootScope.selectedSip.E_AUFNR,
          I_VORNR: $stateParams.islemid,
          I_PER_URL: tSigFilename ? tSigFilename : '',
          I_MUS_URL: cSigFilename ? cSigFilename : '',
          I_TLPC: rejectedStatus,
          I_DESC: (description == null || description == '') ? 'X' : description,
          I_NEWSTATUS: statusCode
        }, function(data) {

          nativeBridge.hideLoading();

          if (data.E_SONUC == 'OK') {
            if (statusCode == 'E0011') {
              $state.go('home.search', {
                location: 'replace'
              });
            } else {
              $state.go('home.search', {
                location: 'replace'
              });
            }

          } else {
            nativeBridge.showAlert(data.E_MESSAGE);

          }
        });
      }





    };

    $scope.setStatus = function(status) {

      //if (status == 'E0011') {
      //    updateIslemStatus(status, '');
      //} else {
      //    $scope.selectedStatus = status;
      //    $scope.showDescription = true;
      //}

      if ($rootScope.selectedSip.E_AUART == 'PM01') {

        $scope.selectedStatus = status;
        $scope.showDescription = true;

      } else {

        updateIslemStatus(status, '');

      }



    };

    $scope.saveDescription = function() {
      updateIslemStatus($scope.selectedStatus, $scope.islemDescription);
    };



  }
]);
